'use strict';
const api = require('./api');
require('dotenv').config();

const express = require('express');
const path = require('path');
const rateLimit = require('express-rate-limit')

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();

const limiter = rateLimit({
    windowMs: 1 * 60 * 1000,        // 1 minutes
    max: 3,                         // limit each IP to 100 requests per window
    message: 'Too many requests'    // error message sent to user when max is exceeded 
})

app.use(limiter)

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/report', async (req, res) => {
  var data = await api.main()
  res.send("This is the failed simulation count: " + JSON.stringify(data))
})


app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
