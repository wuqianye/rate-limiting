const axios = require('axios');
require('dotenv').config();

async function get_token() {
  var domain = process.env.DOMAIN;

  var path = '/api/v1/auth/';
  var base_url = 'https://'+domain;
  var route = base_url + path;

  var uid = process.env.UID;
  var sec = process.env.SEC;

  let response

  try {
    response = await axios.post(route, {
      "uid": uid,
      "sec": sec
    });
  } catch (error) {
    console.log(error)
  }


  var data = response.data;

  return data


}

async function get_failed_simulation_count(token) {
  var domain = process.env.DOMAIN
  var path = '/api/v1/report/failed_simulation_count/';
  var base_url = 'https://'+domain;
  var route = base_url + path;

  var header = {
          headers: {'Authorization': "Bearer " + token}
  };

  try {
    var response = await axios.get(
      route,
      header
    );
  } catch (error) {
    console.log(error)
  }

  var data = response.data
  return data
}

module.exports = {
  main: async function main() {
    token_json = await get_token()
    token = token_json["access_token"]
    data = await get_failed_simulation_count(token)
    console.log("This is the failed simulation count")
    console.log(data)
    return data
  }
};
