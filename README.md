# Prerequisites

To run the project make sure you have signed up for the developer program at <a href='https://hacware.com/dev.html'>Hacware</a> in order to gain access to the api.

# Running the Docker app:

<code>docker build . -t nodejs-app</code>

<code>docker run -p 8080:8080 -d nodejs-app</code>

CD into your location where you store your credentials in .env. You will need a DOMAIN, UID, and SEC.

<code>docker cp .env container-id:/usr/src/app/.env</code>

<code>docker restart container-id</code>

Visit via localhost:8080 in your browser to see the app running.
